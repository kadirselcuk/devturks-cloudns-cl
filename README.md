# devturks.cloudns.cl
## Hoş geldiniz
# Açık kaynak lisansı altında Türk geliştiriciler için açılmıştır
Herkese Merhaba Selam, 
Organizasyonumuzu ziyaret eden herkese teşekkür ederim geliştirme ortamında aktif Türk geliştirici ekipleri oluşturuyoruz :) DevTurks-Team ve TurkDevOps Kuruluşlarımız AÇIK KAYNAK LİSANSLI yazılımlar, ve izin verilen erişimler dahilinde herkesin ücretsiz platform ve ekipler kurmasına yardımcı oluyoruz. 
Bizlerde sizler gibi bu işlerin buraya kadar geldiğine çok şaşırmak gerekirdi belki ama asıl meselenin bir sonraki durağın hangi aşamada nasıl süreçleneceği ve nasıl olmalı , sadece siz ne yapmak isterdiniz? Ve daha huzurlu toplumların birbiriyle daha iyi anlaşması ve daha iyi anlaşılır olmasında topluluklar ile çalışırmıydınız? Bizler aslında bunları hayatımızda iyi bir insan olmanın ve iyi işlerde yardımcı olmamız insan karakterine yakışır bir davranış kabul edilmiştir.
Kişisel düşüncelerim var iyi olmanın ve iyi kalmanın nasıl birşey olduğu gibi ve üzerinde uzun uzun düşünüyorum, bazen öyle ki kafamın içinde kaybolacak dereceye gelmek kadar. Ama düşünmeninde bir sınırı olmalıdır diye düşünüyorum neden derseniz; içinden çıkamıyorsunuz bir süre sonra. 
Yani demem o ki değerli arkadaşlar düşüncelerimizi hedefimize odakladığımız sürece birşeyler oluyor tıpki buradaki gibi bir konuda yardımcı olmak :) 
---
